---
layout: handbook-page-toc
title: Mentoring at GitLab
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is mentoring?

Mentor relationships are an opportunity for individuals to learn from someone's personal experience, background, and perspective. These relationships build trust on a team, provide safe space to make mistakes, and encourage both personal and professional development.

### Benefits for the mentor

1. Serve as a [leader in the organization](/handbook/leadership/) by enabling growth for other team members
1. Practice leadership, [effective communication](/handbook/communication/), and [coaching](/handbook/leadership/coaching/) skills
1. Establish yourself as an expert in a field or speciality
1. [Build trust](/handbook/leadership/building-trust/) with team members

### Benefits for the mentee

1. Be encouraged to prioritize, and be held accountable for, your [career development](/handbook/people-group/learning-and-development/career-development/)
1. Learn new skills related to your current role, your future career goals, or an area that you're passionate about
1. Set and reach clearly outlined [goals](/company/okrs/).

## Find a mentor

The following team members are available as mentors. Schedule a [coffee chat](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) to get the conversation started!

<%= mentors %>

## Become a mentor

1. Team members can designate their availability to be a mentor on the GitLab team page by setting the `mentor` status to `true`. This will appear on the team page with a line that reads `Available as a mentor`. [Example MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/75890)

## Mentoring guidelines

Adapted from the [NCWIT Mentoring-in-a-Box Mentoring Basics - A Mentor's Guide to Success](https://www.ncwit.org/sites/default/files/legacy/pdf/IMentor_MentorGuide.pdf), section What Are the "Dos" of Mentoring:
- Do: Be clear on where the line is drawn between your responsibilities and those of their manager.
- Do: Agree on goals for the mentoring relationship from the outset, and put them in writing. Frequently go back to your goals to measure progress.
- Do: Remember that mentoring is a process with a goal. Have a fun relationship but don’t get off track and lose sight of goals.
- Do: Act as a colleague first, an expert second. Strike an open and warm tone so your mentee will feel they can ask you difficult questions and
take risks. Listen as much as you speak so that their questions and aspirations are always the central focus.
- Do: Set realistic expectations. You can provide your mentee access to resources and people, but make it clear
you do not wield your influence over others – coach as you can but the mentee needs to do their own work.
- Do: Listen, listen, and then listen some more. Hear the concerns of your mentee before offering advice and
guidance. Establish [trust](/handbook/leadership/building-trust/) and openness in communication from the start.
- Do: Recognise that the mentee goals are their own and that they may have career goals that differ from the
path you chose. Your role as a mentor is to guide; it’s up to the mentee to decide what to implement in their own career.
- Do: Recognise that women and other minorities within the organisation do sometimes face additional barriers to advancement.
Educate yourself about the issues. If you want to learn more, ask for advice and support via the [appropriate Diversity, Inclusion and Belonging channels](https://about.gitlab.com/company/culture/inclusion/erg-guide/#how-to-join-current-tmrgs-and-their-slack-channels).
- Do: Keep an open mind. If you are a man mentoring a woman, or if a mentee is from a different ethnic group, be
aware and respect their experiences, ideas, and goals. Cross-gender and cross-cultural mentoring relationships
can be very enriching and successful but it requires open dialogue about the ways gender and culture influence
your mentee's work in the organisation and the mentoring relationship itself.
- Do: Educate others within the organisation about the advancement of women and other under-represented groups. Approach managers and other team members
and mentor them on being effective managers or colleagues to those who might have different experiences to them.
- Do: Teach your mentee how to become a mentor themselves – by example and by encouragement.

## Where is mentoring being discussed in slack?

Mentoring is being discussed in the `#mentoring` slack channel. If you become a mentor or mentee it is recommended that you also join this channel.

## Department-based mentorship programs

Many departments in the GitLab organization organize mentor programs. Check out the current and past programs on the following pages:

1. The [Engineering deparment](/handbook/engineering/) outlines [strategies for mentors and mentees](/handbook/engineering/career-development/mentoring/) with suggestions on how to host meetings and set and evaluate goals.
1. The [Minorities in Tech TMRG](/company/culture/inclusion/erg-minorities-in-tech/) organized a [mentor program](/company/culture/inclusion/erg-minorities-in-tech/mentoring/) with the goal of enabling GitLab to support its Diversity value. Consider using their [program structure](/company/culture/inclusion/erg-minorities-in-tech/mentoring/program-structure/) to organize a mentor program for your team or TMRG.
1. The [Sales department](/handbook/sales/) organized a pilot [Women in Sales Mentorship Program](/handbook/people-group/women-in-sales-mentorship-pilot-program/#women-in-sales-mentorship-program-pilot). The program benefits are outlined [here](/handbook/people-group/women-in-sales-mentorship-pilot-program/#program-benefits).
1. The [Support team](/handbook/support/) has outlined expectations and examples on [Mentorship in Support Engineering](/handbook/support/engineering/mentorship.html).